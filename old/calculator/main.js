function isOperator(char) {
    /**
     * Метод проверки символа на операнта
     * возвращает True если символ один из математических операндов
     */
    return char === '+' || char === '-' || char === '*' || char === '/' || char === '^'
}

function checkDegree(char) {
    /**
     * Метод проверки что символ является математической степенью
     * возвращает True / False
     */
    return char === '^'
}

function getPriorityOperator(char) {
    /**
     * Метод получения приоритета математического операнда
     * возварщает число 0..3
     * чем выше число тем выше приоритет операции
     */
    switch (char) {
        case '^':
            return 3
        case '*':
        case '/':
            return 2
        case '+':
        case '-':
            return 1
        default:
            return 0
    }
}

function convertExpressionToRPL(expression) {
    /**
     * Магический метод преобразования обычного математического выражения в Обратно Польскую Запись
     * на входе задается строковый тип
     * на выходе строка в виде Обратно Польской Записи
     * или Ошибка
     * алгоритм создан согласно статьи
     * https://ru.wikipedia.org/wiki/%D0%9E%D0%B1%D1%80%D0%B0%D1%82%D0%BD%D0%B0%D1%8F_%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F_%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C#%D0%9F%D1%80%D0%B8%D0%BC%D0%B5%D1%80%D1%8B_%D1%80%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8
     */
    if (expression.length === 0) return 'Error in expression'
    let index = 0
    
    //стрелочная функция получения последующего ключа виде числа(дробного), математического оператора или же скобок ()
    let nextToken = function() {
        while (index < expression.length && expression[index] === ' ') index++
        if (index === expression.length) return ''
        let b = ''
        while (index < expression.length
            && expression[index] !== ' '
            && expression[index] !== '('
            && expression[index] !== ')'
            && !isOperator(expression[index])) b += expression[index++]
        if (b !== '') {
            return isFinite(b) ? b : false;}
        return expression[index++]
    }
    
    let Stack = []
    let OutPut = []
    let Token
    
    while ((Token = nextToken()) !== '') {
        if (!Token ) return 'Error in Expression'
        if (Token === '(') Stack.push(Token)
        else if (Token === ')') {
            while (Stack.length > 0 && Stack[Stack.length - 1] !== '(') OutPut.push(Stack.pop())
            if (Stack.length === 0) return 'Error in Expression'
            Stack.pop()
        } else if (isOperator(Token)) {
            while (Stack.length > 0 && isOperator(Stack[Stack.length - 1]) 
                && ((!checkDegree(Token) && getPriorityOperator(Token) <= getPriorityOperator(Stack[Stack.length - 1]))
                || (checkDegree(Token) && getPriorityOperator(Token) < getPriorityOperator(Stack[Stack.length - 1])))) OutPut.push(Stack.pop())
            
            Stack.push(Token)
        } else {
            OutPut.push(Token)
        }
    }
    
    while (Stack.length > 0) {
        if (!isOperator(Stack[Stack.length - 1])) return 'Error in Expression'
        OutPut.push(Stack.pop())
    }
    
    if (OutPut.length === 0) return 'Error in Expression'
    
    let result = ''
    for (let j = 0; j < OutPut.length; j++) {
        if (j !== 0) result += ' '
        result += OutPut[j]
    }
    return result
}

/**
 * Пример подсчета Обратной Польской Записи
 * взят с wiki https://ru.wikiversity.org/wiki/%D0%9E%D0%B1%D1%80%D0%B0%D1%82%D0%BD%D0%B0%D1%8F_%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B0%D1%8F_%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C:_%D0%BF%D1%80%D0%B8%D0%BC%D0%B5%D1%80%D1%8B_%D1%80%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8
 * дополнен оператором возведение в степень
 * и добавлен обработчик события на не возможность выполнения математического выражения
 */
const operators = {
    '+': (x, y) => x + y,
    '-': (x, y) => x - y,
    '*': (x, y) => x * y,
    '/': (x, y) => x / y,
    '^': (x, y) => x ** y
}

let evaluate = (expr) => {
    let stack = []

    expr.split(' ').forEach((token) => {
        if (token in operators) {
            let [y, x] = [stack.pop(), stack.pop()]
            try {
                let result = operators[token](x, y)
                if (!isFinite(result)) {
                    throw new Error()
                }
                stack.push(result)
            } catch (e) {
                console.log("Error: it is not possible to perform a mathematical operation " + token + " with " + x + " and " + y)
                // break
            }

        } else {
            stack.push(parseFloat(token))
        }
    })

    return stack.pop()
}
// Конец кода из википедии


function getVariables(arr) {
    /**
     * Метод получения объекта переменных
     * в виде key:value
     * путем разбивки каждого еллемента входящего массива через разделения по символу =
     */
    let variablesSet = {}
    for (let i=1; i<arr.length; i++){
        let key = arr[i].split('=')[0]
        variablesSet[key] = arr[i].split('=')[1]
    }
    return variablesSet
}

function sortVariablesByLengthKey(src) {
    /**
     * Метод сортировки переменных по длине ключа в обратном порядке от наибольшего размера к наименьшему
     */
    let keys = Object.keys(src)

    keys.sort(function (a, b) {
        return b.length - a.length
    })
    let result = {}
    for (let i=0; i < keys.length; i++) {
        let key = keys[i]
        result[key] = src[key]
    }
    return result
}

function replaceVariableInExpression (src, expression='') {
    /**
     * Метод замены переменных на значения в входящем выражении
     */
    let keys = Object.keys(src)
    expression = expression.replace(' ','')
    expression = expression.replace(',','.')

    for (let i=0; i<keys.length; i++) {
        let key=keys[i]
        let value=src[key]

        while (expression.search(key)>0) {
            if (isFinite(expression[expression.search(key)-1]) || isFinite(expression[expression.search(key)+key.length]))
                return 'Error in Expression'
            expression = expression.replace(key, value)
        }

    }
    return expression
}

//Пример входящих данных
let example = ['21.5+a+((a+ab)*2-(ccc/1))^0.5',
    'a=2',
    'ab=3',
    'ccc=4'
]

let expression = example[0]
let variables = getVariables(example)


console.log('Входящие выражение: ', expression)
console.log('Список переменных: ', variables)
variables = sortVariablesByLengthKey(variables)
expression = replaceVariableInExpression(variables, expression)
console.log('Полученное математическое выражение: ',expression)

console.log('Математическое выржение в виде Обратно Польской Записи: ', convertExpressionToRPL(expression))
console.log('Результат =', evaluate(convertExpressionToRPL(expression)) || 'Ooops')
